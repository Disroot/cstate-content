---
title: 🔧️ Maintenance window 55 🔧️
date: 2023-12-13 21:00:00 
informational: True
section: issue
pin: False
---
On Wednesday starting from 21:00 CET we are going to do some maintenance work. We will make sure to keep downtime as minimal as possible. 
Following services will be affected:
  - Webmail (https://webmail.disroot.org)
  - Nextcloud - (https://cloud.disroot.org)
  - Etherpad (https://pad.disroot.org)
  - PrivateBin - (https://bin.disroot.org)
  - Forgejo - (https://git.disroot.org)

