---
title: New server deployment
date: 2022-11-13 11:00:00 
informational: true
section: issue
pin: false
---

13th November 2022 we are planning to deploy new database server. This means throughout the day you may notice short downtime of some services as we migrate data to the new server. 
We plan to do it service by service which will lower the impact on availability. 
Unfortunately Nextcloud service because of it's size which will take a considerable amount of time (could be hours).

You can follow the process and get all the latest updates by checking our [mastodon](https://social.weho.st/@disroot)
