---
title: 🔧️ Maintenance window 53 🔧️
date: 2023-11-14 21:00:00 
informational: True
section: issue
pin: False
---
On Tuesday starting from 21:00 CET we are going to do some maintenance work. We will make sure to keep downtime as minimal as possible. 
Following services will be affected:
  - Webmail (https://webmail.disroot.org)
  - Nextcloud - (https://cloud.disroot.org)
  - XMPP Chat Server - (XMPP)
  - XMPP Webchat - (https://webchat.disroot.org)
  - Akkoma - (https://fe.disroot.org)

