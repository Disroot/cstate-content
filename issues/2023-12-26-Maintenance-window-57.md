---
title: 🔧️ Maintenance window 57 🔧️
date: 2023-12-26 21:00:00 
informational: True
section: issue
pin: False
---
On Tuesday (2023-12-26) starting from 21:00 CET we are going to do some maintenance work. We will make sure to keep downtime as minimal as possible. 
Following services will be affected:
  - XMPP Webchat - (https://webchat.disroot.org)
  - Lufi - (https://upload.disroot.org)
  - Cryptpad - (https://cryptpad.disroot.org)

