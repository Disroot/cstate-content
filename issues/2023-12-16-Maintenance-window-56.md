---
title: 🔧️ Maintenance window 56 🔧️
date: 2023-12-16 23:00:00 
informational: True
section: issue
pin: False
---
On Saturday (2023-12-16) starting from 23:00 CET we are going to do some maintenance work. We will make sure to keep downtime as minimal as possible. 
Following services will be affected:
  - Nextcloud - (https://cloud.disroot.org)

