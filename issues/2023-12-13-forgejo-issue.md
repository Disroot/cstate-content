---
title: '🔥️ Issue - Git (forgejo) instance is down! 🔥️'
date: 2023-12-13 23:34:00
informational: False
section: issue
severity: down
resolved: true
resolvedWhen: 2023-12-14 2:32:00
pin: False
affected:
   - Git
---
